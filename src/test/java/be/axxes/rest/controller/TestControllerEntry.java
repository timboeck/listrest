package be.axxes.rest.controller;

import org.junit.Test;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TestControllerEntry extends BaseControllerIT {

    @Test
    public void testGetAll() throws Exception {
        performGetJson("/entries")
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value("1"))
                .andExpect(jsonPath("$[0].value").exists())

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value("2"))
                .andExpect(jsonPath("$[1].value").exists())

                .andExpect(jsonPath("$[2]").exists())
                .andExpect(jsonPath("$[2].id").value("3"))
                .andExpect(jsonPath("$[2].value").exists())

                .andExpect(jsonPath("$[3]").exists())
                .andExpect(jsonPath("$[3].id").value("4"))
                .andExpect(jsonPath("$[3].value").exists())

                .andExpect(jsonPath("$[4]").exists())
                .andExpect(jsonPath("$[4].id").value("5"))
                .andExpect(jsonPath("$[4].value").exists())

                .andExpect(jsonPath("$[5]").doesNotExist())
        ;
    }

    @Test
    public void testGetPage1() throws Exception {
        performGetJson("/entries?limit=2")
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value("1"))
                .andExpect(jsonPath("$[0].value").exists())

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value("2"))
                .andExpect(jsonPath("$[1].value").exists())

                .andExpect(jsonPath("$[2]").doesNotExist())
        ;
    }

    @Test
    public void testGetPage2() throws Exception {
        performGetJson("/entries?limit=2&offset=2")
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value("3"))
                .andExpect(jsonPath("$[0].value").exists())

                .andExpect(jsonPath("$[1]").exists())
                .andExpect(jsonPath("$[1].id").value("4"))
                .andExpect(jsonPath("$[1].value").exists())

                .andExpect(jsonPath("$[2]").doesNotExist())
        ;
    }

    @Test
    public void testGetPage3() throws Exception {
        performGetJson("/entries?limit=2&offset=4")
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0]").exists())
                .andExpect(jsonPath("$[0].id").value("5"))
                .andExpect(jsonPath("$[0].value").exists())

                .andExpect(jsonPath("$[2]").doesNotExist())
        ;
    }
}
