package be.axxes.rest.service;

import be.axxes.rest.model.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EntryServiceImpl implements EntryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EntryServiceImpl.class);

    private Map<String, Entry> entries = new HashMap<String, Entry>();

    @PostConstruct
    public void init() {
        putEntry("1");
        putEntry("2");
        putEntry("3");
        putEntry("4");
        putEntry("5");
    }

    private void putEntry(final String id) {
        Entry entry = new Entry(id, "value of " + id);
        entries.put(entry.getId(), entry);
    }

    @Override
    public List<Entry> getEntries() {
        LOGGER.debug("getting all entries");
        //TODO
        return Collections.emptyList();
    }

    @Override
    public List<Entry> getEntries(final Integer limit, final Integer offset) {
        LOGGER.debug("getting entries {} - {}", limit, offset);
        //TODO
        return Collections.emptyList();
    }

}
