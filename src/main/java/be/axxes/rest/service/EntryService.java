package be.axxes.rest.service;

import be.axxes.rest.model.Entry;

import java.util.List;

public interface EntryService {

    List<Entry> getEntries();

    List<Entry> getEntries(Integer limit, Integer offset);
}
